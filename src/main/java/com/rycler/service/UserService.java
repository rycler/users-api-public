package com.rycler.service;

import com.rycler.model.User;

import java.util.List;
import java.util.UUID;

public interface UserService {

    List<User> getAllUsers();

    User getUser(UUID id);

    User addUser(User user);

    void removeUser(UUID id);

    User updateUser(UUID id, User user);
}

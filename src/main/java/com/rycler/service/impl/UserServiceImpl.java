package com.rycler.service.impl;

import com.rycler.handling.exceptions.UserNotFoundException;
import com.rycler.handling.exceptions.UserNotSavedException;
import com.rycler.model.User;
import com.rycler.repository.UserRepository;
import com.rycler.service.UserService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserRepository userRepository;

    public List<User> getAllUsers() {
        List<User> users = userRepository.findAll();
        LOG.info("getAllUsers(): {}", users);
        return users;
    }

    public User getUser(UUID id) {
        return userRepository.findOne(id).orElseThrow(() -> new UserNotFoundException(id));
    }

    public User addUser(User user) {
        Optional<User> us = userRepository.findByUsernameIgnoreCase(user.getUsername());
        return us.orElseGet(() -> userRepository.save(user).orElseThrow(() -> new UserNotSavedException(user)));
    }

    public void removeUser(UUID id) {
        userRepository.delete(id);
    }

    public User updateUser(UUID id, User user) {
        Optional<User> userOptional = userRepository.findOne(id);
        User oldUserEntry;
        if (userOptional.isPresent())
            oldUserEntry = userOptional.get();
        else
            throw new UserNotFoundException(id);

        if (user.getFirstName() != null)
            oldUserEntry.setFirstname(user.getFirstName());
        if (user.getLastName() != null)
            oldUserEntry.setLastname(user.getLastName());

        return userRepository.save(oldUserEntry).orElseThrow(() -> new UserNotSavedException(user));
    }
}

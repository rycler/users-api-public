package com.rycler.repository;

import com.rycler.model.User;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends Repository<User, UUID> {
    Optional<User> findByUsernameIgnoreCase(String username);

    Optional<User> findOne(UUID id);

    Optional<User> save(User user);

    void delete(UUID id);

    List<User> findAll();
}

package com.rycler;

import com.rycler.model.User;
import com.rycler.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan({"com.rycler"})
public class Application implements CommandLineRunner {
    @Autowired
    private UserService userService;

    public static void main(String[] args) {

        ApplicationContext context = SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... strings) throws Exception {
        userService.addUser(new User("Sylar", "Victor", "Crow"));
        userService.addUser(new User("Beast123", "John", "Smith"));
        userService.addUser(new User("Bear", "Bear", "Grylls"));
        userService.addUser(new User("Kitty", "Anna", "Johnes"));
    }
}

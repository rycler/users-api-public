package com.rycler.handling.exceptions;

import com.rycler.model.User;

public class UserNotSavedException extends UserException {
    public UserNotSavedException(User user) {
        super("Not able to save user: " + user.toString());
    }
}

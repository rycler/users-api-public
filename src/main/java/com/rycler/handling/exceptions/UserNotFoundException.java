package com.rycler.handling.exceptions;

import java.util.UUID;

public class UserNotFoundException extends UserException {
    public UserNotFoundException(UUID uuid) {
        super("User with id: " + uuid.toString() + " was not found");
    }
}

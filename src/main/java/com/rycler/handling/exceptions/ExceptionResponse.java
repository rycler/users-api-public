package com.rycler.handling.exceptions;

public class ExceptionResponse {
    String message;
    String details;

    public ExceptionResponse(String message, String details) {
        this.message = message;
        this.details = details;
    }

    public String getMessage() {
        return message;
    }

    public String getDetails() {
        return details;
    }
}

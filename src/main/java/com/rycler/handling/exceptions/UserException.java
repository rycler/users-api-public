package com.rycler.handling.exceptions;

class UserException extends RuntimeException {

    UserException(String userExceptionMessage) {
        super(userExceptionMessage);
    }
}

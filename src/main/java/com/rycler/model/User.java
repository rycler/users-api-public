package com.rycler.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Size;
import java.util.UUID;

@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    @Size(min=2,message="Username must have minimum 2 characters")
    private String username;
    @Size(min=2,message="FirstName must have minimum 2 characters")
    @JsonProperty("firstname")
    private String firstname;
    @Size(min=2,message="LastName must have minimum 2 characters")
    @JsonProperty("lastname")
    private String lastname;

    protected User() {
    }

    public User(String username, String firstname, String lastname) {
        this.username = username;
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public String getUsername() {
        return username;
    }

    @JsonProperty("lastname")
    public String getLastName() {
        return lastname;
    }

    @JsonProperty("firstname")
    public String getFirstName() {
        return firstname;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    @JsonProperty("firstname")
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    @JsonProperty("lastname")
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

}

package com.rycler.controller;

import com.rycler.model.User;
import com.rycler.service.UserService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "${context.path:}")
public class Controller {
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(Controller.class);

    @Autowired
    private UserService userService;

    @CrossOrigin
    @PostMapping(value = "/users", consumes = "application/json; charset=utf8", produces = "application/json; charset=utf8")
    public ResponseEntity<User> addUser(@Valid @RequestBody User user) throws URISyntaxException {
        LOG.info("Starting addUser()");
        User newUser = userService.addUser(user);
        URI responseURI = ServletUriComponentsBuilder.fromCurrentRequestUri().buildAndExpand(newUser.getId()).toUri();
        LOG.info("Completed addUser()");

        return ResponseEntity.created(responseURI).body(newUser);
    }

    @CrossOrigin
    @PatchMapping(value = "/users/{paramId}", consumes = "application/json; charset=utf8", produces = "application/json; charset=utf-8")
    public ResponseEntity<User> patchUser(@PathVariable UUID paramId,@Valid @RequestBody User user) {
        LOG.info("Starting patchUser()");
        return ResponseEntity.status(HttpStatus.CREATED).body(userService.updateUser(paramId, user));
    }

    @CrossOrigin
    @GetMapping(value = "/users/{paramId}", produces = "application/json; charset=utf-8")
    public ResponseEntity<User> getUser(@PathVariable UUID paramId) {
        LOG.info("Starting getUser()");
        return ResponseEntity.ok().body(userService.getUser(paramId));
    }

    @CrossOrigin
    @GetMapping(value = "/users", produces = "application/json; charset=utf-8")
    public ResponseEntity<List<User>> getUsers() {
        LOG.info("Starting getUsers()");
        return ResponseEntity.ok().body(userService.getAllUsers());
    }

    @CrossOrigin
    @DeleteMapping(value = "/users/{paramId}", produces = "application/json; charset=utf8")
    public ResponseEntity<Void> removeUser(@PathVariable UUID paramId) {
        LOG.info("Starting removeUser()");
        userService.removeUser(paramId);
        return ResponseEntity.noContent().build();
    }

    //Allow diferent origin request so JQUERY wont throw error after trying to post
    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").allowedOrigins("*");
            }
        };
    }

}

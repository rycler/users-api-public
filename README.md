# Users API

Simple API example with users.

### Prerequisites

What things you need to install the software and how to install them

```
Java
Maven
```

## Running the tests

* [Postman script](https://www.getpostman.com/collections/3f9b25fe6668e707097d)

## Deployment

mvn clean package  

## Run the application

java -jar target\us-users-api-1.0-SNAPSHOT.jar


## Built With

* [Spring](https://spring.io/) - The Spring framework used
* [Maven](https://maven.apache.org/) - Dependency Management

## Versioning

Project is an ALPHA stage.

## Authors

*Initial work* - [rycler](https://gitlab.com/rycler)  
